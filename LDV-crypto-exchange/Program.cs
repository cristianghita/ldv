﻿using LDV_crypto_exchange.model;
using LDV_crypto_exchange.Utils;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace LDV_crypto_exchange
{
    class Program
    {
        private static int _pingLdvStatusInterval = 5;

        /// <summary>
        /// 1. Loads config file from passed args or defauls to /bin/debug/config.json
        /// 2. Defines 3 providers based on config file (can adapt to unlimited number of providers if needed)
        /// 3. Every _pingLdvStatusInterval seconds checks started providers 
        /// 3.1 If provider ticked over 10 times send provider status to LDV
        /// 3.2 Every second ticks started providers
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            var config = LoadConfig(args.Length > 0 ? args[0] : "config.json");
            var coinbaseProvider = new Provider(
                name:           "Coinbase",
                pulseInterval:  config.PulseInterval, 
                pingUri:        config.CoinbasePingUri,
                okUri:          config.LdvCoinbaseOk,
                faultyUri:      config.LdvCoinbaseFaulty,
                downUri:        config.LdvCoinbaseDown);

            var krakenProvider = new Provider(
                name:                "Kraken",
                pulseInterval:       config.PulseInterval,
                pingUri:             config.KrakenPingUri,
                okUri:               config.LdvKrakenOk,
                faultyUri:           config.LdvKrakenFaulty,
                downUri:             config.LdvKrakenDown,
                alternativeProvider: coinbaseProvider,
                cluster:             config.KrakenCluster);

            var binanceProvider = new Provider(
                name:                "Binance",
                pulseInterval:       config.PulseInterval, 
                pingUri:             config.BinancePingUri,
                okUri:               config.LdvBinanceOk,
                faultyUri:           config.LdvBinanceFaulty,
                downUri:             config.LdvBinanceDown,
                alternativeProvider: krakenProvider,
                cluster:             config.BinanceCluster);

            /* Uncomment to test failover providers */
            //binanceProvider._hardcodeTimeout = 0;
            //krakenProvider._hardcodeTimeout = 0;

            while (true)
            {
                StartAndCheckProvider(binanceProvider);
                for (var i = 0; i < _pingLdvStatusInterval; i++)
                {
                    binanceProvider.TickProvider();
                    krakenProvider.TickProvider();
                    coinbaseProvider.TickProvider();

                    System.Threading.Thread.Sleep(1000);
                }
            }
        }

        /// <summary>
        /// Checks a provider. 
        /// Status != Undefined > send provider status to LDV
        /// Status == Faulty || Down > check its alternative provider.
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        static void StartAndCheckProvider(Provider provider)
        {
            StartProviderAsync(provider);
            var providerStatus = provider.GetProviderStatus();
            if (providerStatus == ProviderStatus.Undefined)
                return;

            var alternativeProvider = provider.GetAlternativeProvider();
            if (providerStatus == ProviderStatus.Ok)
            {
                provider.LdvGetOkProvider();
                if (alternativeProvider != null)
                    StopProviderAsync(alternativeProvider);
            }
            else
            {
                if (providerStatus == ProviderStatus.Faulty)
                    provider.LdvGetFaultyProvider();
                else
                    provider.LdvGetDownProvider();

                if (alternativeProvider != null)
                    StartAndCheckProvider(alternativeProvider);
            }
        }

        private static void StartProviderAsync(Provider provider)
        {
            Task.Run(() => provider.StartProvider());
        }

        private static void StopProviderAsync(Provider provider)
        {
            Task.Run(() => provider.StopProvider());
            if (provider.GetAlternativeProvider() != null)
            {
                StopProviderAsync(provider.GetAlternativeProvider());
            }
        }

        private static Config LoadConfig(string configPath)
        {
            using (var reader = new StreamReader(configPath))
            {
                var configAsJson = reader.ReadToEnd();
                return JsonUtil.ParseConfig(configAsJson);
            }
        }
    }
}
