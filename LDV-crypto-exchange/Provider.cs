﻿using LDV_crypto_exchange.model;
using LDV_crypto_exchange.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace LDV_crypto_exchange
{
    public class Provider
    {
        private string _name;
        private int _pulseInterval;
        private string _pingUri;
        private string[] _cluster;
        private ProviderStatus _providerStatus = ProviderStatus.Undefined;
        private List<bool> _lastPings = new List<bool>();
        private bool _isProviderStarted;
        private Provider _alternativeProvider;
        private string _okUri;
        private string _faultyUri;
        private string _downUri;
        public int? _hardcodeTimeout;

        public Provider(string name, int pulseInterval, string pingUri, string okUri, string faultyUri, string downUri, Provider alternativeProvider = null, string[] cluster = null)
        {
            _name = name;
            _pulseInterval = pulseInterval;
            _pingUri = pingUri;
            _okUri = okUri;
            _faultyUri = faultyUri;
            _downUri = downUri;
            _alternativeProvider = alternativeProvider;
            _cluster = cluster;
        }

        public Provider GetAlternativeProvider()
        {
            return _alternativeProvider;
        }

        public void LdvGetOkProvider()
        {
            // LDV - GET -> Ok -- use _okUri
            System.Console.WriteLine($"OK - [{_name}]");
        }

        public void LdvGetFaultyProvider()
        {
            // LDV - GET -> Ok -- use _faultyUri
            System.Console.WriteLine($"Faulty - [{_name}]");
        }

        public void LdvGetDownProvider()
        {
            // LDV - GET -> Ok -- use _downUri
            System.Console.WriteLine($"Down - [{_name}]");
        }

        public ProviderStatus GetProviderStatus()
        {
            return _providerStatus;
        }

        public async Task StopProvider()
        {
            if (!_isProviderStarted) return;
            System.Console.WriteLine($"[{_name}] STOP");
            _lastPings = new List<bool>();
            _isProviderStarted = false;
        }

        public void TickProvider()
        {
            if (_isProviderStarted)
            {
                System.Console.WriteLine($" TICK - [{_name}]");
                var isPingSuccesful = IsStatusOk();
                _lastPings.Add(isPingSuccesful);

                if (_lastPings.Count > 10)
                    _lastPings.RemoveAt(0);

                if (_lastPings.Count < 10)
                    _providerStatus = ProviderStatus.Undefined;
                else
                {
                    var okPings = _lastPings.Count(x => x == true);
                    _providerStatus =
                        okPings > 8
                        ? ProviderStatus.Ok
                            : okPings > 3
                            ? ProviderStatus.Faulty
                                : ProviderStatus.Down;
                }
            }
        }

        public async Task StartProvider()
        {
            if (_isProviderStarted) return;

            Console.WriteLine($"[{_name}] START");
            _isProviderStarted = true;
        }

        private bool IsStatusOk()
        {
            return WebUtil.IsStatusCodeOk(GetStatusCode());
        }

        private int GetStatusCode()
        {
            try
            {
                if (_hardcodeTimeout != null)
                {
                    return WebUtil.GETStatusCode(_pingUri, (int) _hardcodeTimeout);
                }

                if (_cluster == null || _cluster.Length == 0)
                {
                    return WebUtil.GETStatusCode(_pingUri, 5000);
                }

                return WebUtil.GETStatusCode(_pingUri);
            }
            catch (WebException ex)
            {
                if (ex.Status != WebExceptionStatus.Timeout) throw ex; // only catch timeout - main api might have performance issues
                if (_cluster == null || _cluster.Length == 0) return -1;
                return TryCluster(_cluster);
            }
        }

        private int TryCluster(string[] cluster)
        {
            var apiAlternative = 0;
            while (apiAlternative < cluster.Length)
            {
                var timeout = _hardcodeTimeout != null ? (int) _hardcodeTimeout : apiAlternative + 1 == cluster.Length ? 5000 : 2000; // final attempt has bigger timeout
                try
                {
                    return WebUtil.GETStatusCode(cluster[apiAlternative], timeout);
                }
                catch (WebException ex)
                {
                    if (ex.Status != WebExceptionStatus.Timeout && ex.Status != WebExceptionStatus.SecureChannelFailure) throw ex; 
                    apiAlternative++;
                }
            }
            return -1;
        }
    }
}
