﻿using LDV_crypto_exchange.model;
using Newtonsoft.Json;
using System.IO;

namespace LDV_crypto_exchange.Utils
{
    public static class JsonUtil
    {
        public static Config ParseConfig(string configAsString)
        {
            return JsonConvert.DeserializeObject<Config>(configAsString);
        } 
    }
}
