﻿using System;
using System.IO;
using System.Net;

namespace LDV_crypto_exchange.Utils
{
    public static class WebUtil
    {
        private const int _defaultTimeout = 2000;

        public static HttpWebResponse GET(string uri, int timeout = _defaultTimeout)
        {
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Timeout = timeout;

            return (HttpWebResponse)request.GetResponse();
        }

        public static string GETStringResponse(string uri, int timeout = _defaultTimeout)
        {
            using (var response = GET(uri, timeout))
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }

        public static int GETStatusCode(string uri, int timeout = _defaultTimeout)
        {
            using (var response = GET(uri, timeout))
            {
                return (int)response.StatusCode;
            }   
        }

        public static bool IsStatusCodeOk(int statusCode)
        {
            return statusCode == 200;
        }
    }
}
