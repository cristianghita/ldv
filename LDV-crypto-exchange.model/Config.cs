﻿namespace LDV_crypto_exchange.model
{
    public class Config
    {
        public int PulseInterval;

        #region Binance

        public string BinancePingUri;
        public string[] BinanceCluster;

        public string LdvBinanceOk;
        public string LdvBinanceFaulty;
        public string LdvBinanceDown;

        #endregion

        #region Kraken

        public string KrakenPingUri;
        public string[] KrakenCluster;

        public string LdvKrakenOk;
        public string LdvKrakenFaulty;
        public string LdvKrakenDown;

        #endregion

        #region Coinbase

        public string CoinbasePingUri;

        public string LdvCoinbaseOk;
        public string LdvCoinbaseFaulty;
        public string LdvCoinbaseDown;

        #endregion
    }
}
