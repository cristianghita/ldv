﻿namespace LDV_crypto_exchange.model
{
    public enum ProviderStatus
    {
        Undefined = 0,
        Ok = 1,
        Faulty = 2,
        Down = 3
    }
}
